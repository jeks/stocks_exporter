FROM elixir:1.12

COPY . /app
WORKDIR /app

# ARG DB_URL
# ENV DB_URL $DB_URL
ENV MIX_ENV=prod

RUN mix local.hex --force && mix local.rebar --force
RUN mix do deps.get, deps.compile, compile

ENTRYPOINT ["mix", "run", "--no-halt"]
