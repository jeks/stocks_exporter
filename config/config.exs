import Config

config :bot, ecto_repos: [Repo]
config :bot, Repo,
       url: System.get_env("DB_URL")

config :logger, :console, metadata: [:request_id]

import_config "#{config_env()}.exs"