defmodule Adapter.Ozon do
  use GenServer
  use HTTPoison.Base
  require Logger
  import Ecto.Query

  @endpoint "https://api-seller.ozon.ru"
  def process_response_body(body) do
    case Jason.decode(body) do
      {:ok, resp} -> resp
      {:error, err} -> %{error: [err, body]}
    end
  end

  def start_link(key) do
    GenServer.start_link(__MODULE__, key, name: process_name(key.id))
  end

  def process_name(key_id), do: {:via, Registry, {Bot.Registry, "bot_#{key_id}"}}

  def init(state) do
    Process.send_after(self(), :work, 1)
    {:ok, state}
  end

  def handle_info(:work, %Schema.Key{id: key_id} = state) do
    work(state)
    %Schema.Key{refresh_interval_secs: interval} = Repo.one from k in Schema.Key,
                                              where: k.id == ^key_id,
                                              select: k

    Process.send_after(self(), :work, interval * 1_000)

    {:noreply, state}
  end

  defp work(%Schema.Key{body: api_key, id: key_id, params: %{"Client-Id" => client_id}}) do
    headers = [
      {"User-Agent", "Curl"},
      {"Client-Id", client_id},
      {"Api-Key", api_key},
      {"Content-Type", "application/json"}
    ]
    case __MODULE__.post(@endpoint <> "/v1/analytics/stock_on_warehouses", "{\"limit\":10000, \"offset\": 0}", headers) do
      {:ok, %HTTPoison.Response{body: %{error: err}}} ->
        Logger.error inspect err
      {:ok, %HTTPoison.Response{body: body}} ->
        stocks = Enum.reduce(body["wh_items"], [], fn(wh, acc) ->
          acc ++ Enum.map(wh["items"], fn(w) ->
            %Schema.StockForSale{
              time: NaiveDateTime.truncate(NaiveDateTime.utc_now(), :second),
              barcodes: String.split(w["barcode"], ";", trim: true),
              title: w["title"],
              warehouse: "ozon_"<> wh["id"],
              mp_sku: to_string(w["sku"]),
              seller_sku: to_string(w["offer_id"]),
              val: w["stock"]["for_sale"],
              key_id: key_id
            }
          end)
        end)
        Repo.insert_stocks_by_key(stocks, key_id)
      err ->
        Logger.error inspect err
    end
  end

end