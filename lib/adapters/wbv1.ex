defmodule Adapter.Wbv1 do
  use GenServer
  use HTTPoison.Base
  require Logger
  import Ecto.Query

  @endpoint "https://suppliers-stats.wildberries.ru"
  def process_response_body(body) do
    case Jason.decode(body) do
      {:ok, resp} -> resp
      {:error, err} -> %{error: [err, body]}
    end
  end

  def start_link(key) do
    GenServer.start_link(__MODULE__, key, name: process_name(key.id))
  end

  def process_name(key_id), do: {:via, Registry, {Bot.Registry, "bot_#{key_id}"}}

  def init(state) do
    Process.send_after(self(), :work, 1)
    {:ok, state}
  end

  def handle_info(:work, %Schema.Key{id: key_id} = state) do
    work(state)
    %Schema.Key{refresh_interval_secs: interval} = Repo.one from k in Schema.Key,
                                                            where: k.id == ^key_id,
                                                            select: k
    Process.send_after(self(), :work, interval * 1_000)

    {:noreply, state}
  end

  # TODO recursive page fetch
  defp work(%Schema.Key{body: api_key, id: key_id}) do
    headers = [ {"User-Agent", "Curl"}, {"Content-Type", "application/json"} ]
    case __MODULE__.get(@endpoint <> "/api/v1/supplier/stocks?dateFrom=2021-01-01T01:00:00.000Z&key=#{api_key}", headers) do
      {:ok, %HTTPoison.Response{body: %{error: err}}} ->
        Logger.error inspect err
      {:ok, %HTTPoison.Response{body: body}} ->
        stocks = Enum.map body, fn w ->
          %Schema.StockForSale{
            time: NaiveDateTime.truncate(NaiveDateTime.utc_now(), :second),
            barcodes: String.split(w["barcode"], ";", trim: true),
            title: w["subject"],
            warehouse: "wb_" <> w["warehouseName"],
            seller_sku: to_string(w["supplierArticle"]),
            mp_sku: to_string(w["nmId"]),
            val: (w["quantityNotInOrders"] || w["quantity"]),
            key_id: key_id
          }
        end
        Repo.insert_stocks_by_key(stocks, key_id)
      err ->
        Logger.error inspect err
    end
  end
end