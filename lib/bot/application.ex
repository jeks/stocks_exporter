defmodule Bot.Application do
  use Application
  require Logger

  @impl true
  def start(_type, _args) do
    children = [
       {Repo, []},
       {Key.Supervisor, []},
       {Registry, keys: :unique, name: Bot.Registry},
       {Bot.Supervisor, name: Bot.Supervisor, strategy: :one_for_one},
       {Plug.Cowboy, scheme: :http, plug: Bot.Router, options: [port: 4001]}
    ]

    result = Supervisor.start_link(children, strategy: :one_for_one)
    Bot.Supervisor.start_workers()
    result
  end



end
