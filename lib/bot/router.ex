defmodule Bot.Router do
    use Plug.Router
    use Plug.ErrorHandler

    plug :match
    plug Plug.Telemetry, event_prefix: [:bot, :router]
    plug Plug.RequestId
    plug Plug.Parsers, parsers: [:urlencoded, {:json, json_decoder: Jason}]
    plug Plug.Logger, log: :debug
    plug Plug.Session, store: :ets, key: "_lf_session", table: :sessions
    plug :dispatch

    get "/" do
        send_resp(conn, 200, "Welcome")
    end

#    get "/api/accounts/:email/stocks.csv" do
#        columns = [Enum.join(~w(time warehouse title barcodes stock_for_sale), ";")]
#        rows =
#          Repo.all(Repo.stocks_by_email(email))
#          |> Enum.map(&(Enum.join([NaiveDateTime.to_string(&1.time), &1.warehouse, &1.title, Enum.join(&1.barcodes, ","), &1.stock_for_sale], ";")))
#        result= Enum.join(columns ++ rows, "\n")
#
#        conn
#        |> put_resp_header("content-type", "application/csv; charset=utf-8")
#        |> put_resp_header("content-disposition", "attachment; filename=stocks.csv")
#        |> send_resp(:ok, result)
#    end
#
#    get "/api/stocks/:email" do
#        result = Repo.all Repo.stocks_by_email(email)
#
#        conn
#        |> put_resp_header("content-type", "application/json; charset=utf-8")
#        |> send_resp(200, Jason.encode!(result))
#    end

    match _ do
        send_resp(conn, 404, "not found")
    end

    defp handle_errors(conn, %{kind: _kind, reason: _reason, stack: _stack}) do
        send_resp(conn, conn.status, "Something went wrong")
    end

end
