defmodule Bot.Supervisor do
  import Ecto.Query
  require Logger
  use DynamicSupervisor

  def init(_init_arg) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def start_link(arg) do
    DynamicSupervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def process_name(key_id), do: {:via, Registry, {Bot.Registry, "bot_#{key_id}"}}

  def stop_worker(key_id) do
    case Registry.lookup(Bot.Registry, "bot_#{key_id}") do
      [{pid, _}] -> DynamicSupervisor.terminate_child(__MODULE__, pid)
      d -> Logger.error inspect ["UNKNOWN PID", d]
    end
  end

  def start_worker(key_id) do
    key = Repo.get!(Schema.Key, key_id)
    DynamicSupervisor.start_child(__MODULE__, {bot(key.adapter), key})
  end

  def restart_worker(key_id) do
    case Registry.lookup(Bot.Registry, "bot_#{key_id}") do
      [{pid, _}] -> DynamicSupervisor.terminate_child(__MODULE__, pid)
      d -> Logger.error inspect ["UNKNOWN PID", d]
    end

    key = Repo.get!(Schema.Key, key_id)
    DynamicSupervisor.start_child(__MODULE__, {bot(key.adapter), key})
  end

  def start_workers() do
    query = from u in Schema.User,
                 join: k in Schema.Key, on: k.user_id == u.id,
                 select: k

    Repo.all(query) |>
      Enum.each(fn key -> DynamicSupervisor.start_child(__MODULE__, {bot(key.adapter), key}) end)
  end

  def bot("ozon"), do: Adapter.Ozon
  def bot("wbv1"), do: Adapter.Wbv1
  def bot(_), do: nil

end