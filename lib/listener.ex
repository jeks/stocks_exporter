defmodule Key.Supervisor do
  use GenServer
  require Logger

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]}
    }
  end

  def start_link(opts \\ []), do: GenServer.start_link(__MODULE__, opts)

  def init(opts) do
    with {:ok, _pid, _ref} <- Repo.listen("keys") do
      {:ok, opts}
    else
      error -> {:stop, error}
    end
  end

  def handle_info({:notification, _pid, _ref, "keys", payload}, _state) do
    with {:ok, data} <- Jason.decode(payload) do
      case data do
        %{"operation" => "DELETE", "data" => %{"id" => key_id}} ->
          Bot.Supervisor.stop_worker(key_id)
        %{"operation" => "INSERT", "data" => %{"id" => key_id}} ->
          Bot.Supervisor.start_worker(key_id)
        %{"operation" => "UPDATE", "data" => %{"id" => key_id}} ->
          Bot.Supervisor.restart_worker(key_id)
        _ -> Logger.error inspect ["UNKNOWN", data]
      end

      {:noreply, :event_handled}
    else
      error -> {:stop, error, []}
    end
  end
end