defmodule Repo do
  use Ecto.Repo,
    otp_app: :bot,
    adapter: Ecto.Adapters.Postgres
  require Logger
  import Ecto.Query

  def listen(chan) do
    with {:ok, pid} <- Postgrex.Notifications.start_link(Keyword.put_new(__MODULE__.config(), :auto_reconnect, true)),
         {:ok, ref} <- Postgrex.Notifications.listen(pid, chan) do
      {:ok, pid, ref}
    end
  end

  def insert_stocks_by_key(online, key_id) do
    local = all stocks_by_key(key_id)
    Logger.debug inspect [key_id, "local #{length(local)}"]

    new_stocks =
      Enum.filter(online, fn(o) ->
        (o.val > 0) && is_nil(Enum.find(local, fn(l) -> (o.barcodes == l.barcodes) && (o.warehouse == l.warehouse) end))
      end) |> Enum.map(fn(s) -> Map.drop(s, [:__meta__, :key, :__struct__]) end)
    Logger.debug inspect [key_id, "new stocks #{length(new_stocks)}"]

    updated_stocks =
      Enum.filter(online, fn(o) ->
        Enum.any?(local, fn(l) -> (o.val != l.val) && (o.barcodes == l.barcodes) && (o.warehouse == l.warehouse) end)
      end) |> Enum.map(fn(s) -> Map.drop(s, [:__meta__, :key, :__struct__]) end)
    Logger.debug inspect [key_id, "updated stocks #{length(updated_stocks)}"]

    zero_stocks = Enum.filter(local, fn(l) ->
      (l.val > 0) && is_nil(Enum.find(online, fn(o)->
        (o.warehouse == l.warehouse) && (o.barcodes == l.barcodes)
      end))
    end) |> Enum.map(fn(s) ->
      Map.merge(s, %{time: NaiveDateTime.truncate(NaiveDateTime.utc_now(), :second), val: 0})
    end) |> Enum.map(fn(s) -> Map.drop(s, [:__meta__, :key, :__struct__]) end)
    Logger.debug inspect [key_id, "zero stocks #{length(zero_stocks)}"]

    stocks = new_stocks ++ updated_stocks ++ zero_stocks
    insert_all(Schema.StockForSale, stocks)

  end

  def stocks_by_key(key_id) do
    from s in Schema.StockForSale,
         left_join: s1 in Schema.StockForSale, on: s.warehouse == s1.warehouse and s.barcodes == s1.barcodes and s.time < s1.time,
         join: k in assoc(s, :key),
         where: k.id == ^key_id and is_nil(s1.time),
         select: s
  end
#
#  def stocks_by_email(email) do
#    from s in Schema.StockForSale,
#             left_join: s2 in Schema.StockForSale, on: s.warehouse == s2.warehouse and s.barcodes == s2.barcodes and s.time < s2.time,
#             join: k in assoc(s, :key),
#             join: u in assoc(k, :user),
#             where: a.email == ^email and is_nil(s2.time) and (s.val != 0) ,
#             select: %{time: s.time, barcodes: s.barcodes, title: s.title, warehouse: s.warehouse, stock_for_sale: s.val},
#             order_by: s.warehouse
#  end

end