defmodule Schema.Key do
  use Ecto.Schema

  schema "keys" do
    field :body, :string, null: false
    field :params, :map, null: false
    field :adapter, :string, null: false
    field :refresh_interval_secs, :integer, null: false
    belongs_to :user, Schema.User
  end
end