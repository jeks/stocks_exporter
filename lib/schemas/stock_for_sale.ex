defmodule Schema.StockForSale do
  use Ecto.Schema

  @primary_key false
  schema "stocks_for_sales" do
    field :time,      :naive_datetime
    field :warehouse, :string
    field :barcodes,  {:array, :string}
    field :title,     :string
    field :mp_sku,    :string
    field :seller_sku, :string
    field :val,       :integer
    belongs_to :key, Schema.Key
  end
end