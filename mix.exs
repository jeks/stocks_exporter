defmodule Bot.MixProject do
  use Mix.Project

  def project do
    [
      app: :bot,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Bot.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:plug_cowboy, "~> 2.0"},
      {:httpoison, "~> 1.8"},
      {:postgrex, "~> 0.15.10"},
      {:poison, "~> 3.1"},
      {:jason, "~> 1.2"},
      {:ecto, "~> 3.7"},
      {:ecto_sql, "~> 3.0"}
    ]
  end
end
