defmodule Repo.Migrations.AddAccountsTable do
  use Ecto.Migration

  def change do
    create table("accounts") do
      add :email, :string, null: false
      add :ozon_client_id, :string
      add :ozon_client_key, :string
      add :wb_key, :string
      add :wb_key_type, :string
      add :worker_interval, :integer, null: false, default: 21600

      timestamps()
    end

    create unique_index(:accounts, :email, name: :accounts_email_index)
  end
end
