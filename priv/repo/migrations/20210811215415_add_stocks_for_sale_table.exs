defmodule Repo.Migrations.AddStocksForSaleTable do
  use Ecto.Migration

  def change do
    create table("stocks_for_sales",  primary_key: false) do
      add :time,       :naive_datetime, default: fragment("now()"), null: false
      add :warehouse,  :string, null: false
      add :barcode,   :string, null: false
      add :title,      :string
      add :mp_sku,     :string, null: false
      add :seller_sku, :string, null: false
      add :val,        :integer, null: false
      add :account_id, references(:accounts), null: false
    end
  end
end
