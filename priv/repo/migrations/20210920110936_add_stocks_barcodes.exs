defmodule Repo.Migrations.AddStocksBarcode do
  use Ecto.Migration

  def change do
    alter table "stocks_for_sales" do
      add :barcodes, {:array, :string}, default: [], null: false
    end

    alter table "stocks_for_sales" do
      remove :barcode
    end
  end
end
