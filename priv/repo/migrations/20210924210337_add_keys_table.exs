defmodule Repo.Migrations.AddKeysTable do
  use Ecto.Migration

  def change do
    create table "keys" do
      add :body, :string, null: false
      add :params, :map, null: false
      add :account_id, references(:accounts), null: false

      timestamps()
    end

    alter table "stocks_for_sales" do
      add :key_id, references(:keys), null: false
      remove :account_id, references(:keys), null: false
    end

  end
end
