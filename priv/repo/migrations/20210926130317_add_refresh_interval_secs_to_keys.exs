defmodule Repo.Migrations.AddRefreshIntervalSecsToKeys do
  use Ecto.Migration

  def change do
    alter table "keys" do
      add :refresh_interval_secs, :integer
    end
  end
end
