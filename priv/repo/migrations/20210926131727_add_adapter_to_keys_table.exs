defmodule Repo.Migrations.AddAdapterToKeysTable do
  use Ecto.Migration

  def change do
    alter table "keys" do
      add :adapter, :string, null: false
    end
  end
end
